{pkgs, index, semver, patch}:
let
    index_ = index;
in let
    index = with builtins; rec {
        src = index_;
        findCrate = {name, version}:
            let
                patch_ = if builtins.hasAttr name patch then
                    builtins.getAttr name patch
                else null;
                versionSem = semver.parseSemverRequirement (if patch_ != null && builtins.isAttrs patch_ then
                    patch_.version
                    else version);
                matching = (filter
                    (x: semver.requirementMatches versionSem (elemAt (semver.parseSemver x.vers) 0))
                    (sort (a: b: semver.compareVersions (
                        (elemAt (semver.parseSemver a.vers) 0)
                    ) (
                        (elemAt (semver.parseSemver b.vers) 0)
                    )) (map fromJSON (filter (x: (isString x) && (substring 0 1 x) == "{") (
                        split "\n" (
                            if stringLength name == 3 then
                                readFile "${src}/3/${substring 0 1 name}/${name}"
                            else if stringLength name == 2 then
                                readFile "${src}/2/${name}"
                            else if stringLength name == 1 then
                                readFile "${src}/1/${name}"
                            else
                                readFile "${src}/${substring 0 2 name}/${substring 2 2 name}/${name}"
                            )
                        )
                    )))
                );
            in
            if matching == [] then
                abort "Cannot find crate: ${name}-${version}"
            else
                pkgs.lib.lists.last matching
            ;
    };
    crate = {
        name,
        version,
    }: let
        patch_ = if builtins.hasAttr name patch then
            builtins.getAttr name patch
        else null;
        match = index.findCrate {
            inherit name version;
        };
    in if patch_ != null && !(builtins.isAttrs patch_) then
        patch_
    else pkgs.stdenv.mkDerivation {
        name = "${name}-${match.vers}-source";
        src = pkgs.fetchurl {
            url = "https://crates.io/api/v1/crates/${name}/${match.vers}/download";
            sha256 = match.cksum;
        };
        builder = ./fetchCrate.sh;
        buildInputs = [ pkgs.gnutar ];
    };
in
{
    inherit crate;
    findCrate = index.findCrate;
}