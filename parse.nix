{
    lib ? (import <nixpkgs> {}).lib
}:
with builtins;

let
    char = c: (input: if c (substring 0 1 input) then
            {
                value=(substring 0 1 input);
                input=(substring 1 (stringLength input) input);
            }
        else
            null
    );
    word = w: (input: if (stringLength input >= stringLength w) && (substring 0 (stringLength w) input) == w then
            {
                value=w;
                input=(substring (stringLength w) (stringLength input) input);
            }
        else
            null
    );
    takeWhile = test: (input: let
        check = test input;
    in
        if check != null then
            let
                next = takeWhile test check.input;
            in
            {
                value = [ check.value ] ++ next.value;
                input = next.input;
            }
        else
            {
                value = [];
                input = input;
            }
    );
    andThen = a: b:
        input:
        let
            ares = a input;
            bres = if ares != null then
                b ares.input
            else
                null;
        in
            bres
        ;
    choose = tests:
        input: let
            checks = filter (x: x != null)
                (map (x: x input) tests);
            in
                if checks == [] then null else (head checks)
            ;

    whitespace = char (x: x == " " || x == "\t");
    skipEmpty = takeWhile whitespace;

    countLines = input:
        length (
            filter isString (
                split "\n" input));

    debugPrint = x:
        (debugPrintImpl "" x) + "\n";

    debugPrintImpl = indent: x:
        if isAttrs x then
            ("{\n"
                + foldl' (a: b:
                    a + indent + "    " + b + " = " + (debugPrintImpl (indent + "    ") x.${b}) + ";\n"
                    ) "" (attrNames x)
                + indent + "}")
        else if isString x then
            ''"'' + (lib.strings.escape [''"''] x) + ''"''
        else if isList x then
            ("[\n"
                + foldl' (a: b:
                    a + indent + "    " + (debugPrintImpl (indent + "    ") b) + "\n"
                    ) "" x
                + indent + "]")
        else
            toString x
        ;

    escapedString = escape: input:
        let
            c = char (x: x != escape) input;
        in
            if c == null then
                {
                    value = "";
                    input = input;
                }
            else let
                ch = if c.value == "\\" then
                    char (x: true) c.input
                else
                    c;
                next = escapedString escape ch.input;
            in
                {
                    value = ch.value + next.value;
                    input = next.input;
                }
        ;
in
{
    inherit char word takeWhile andThen choose whitespace skipEmpty countLines debugPrint escapedString;
}