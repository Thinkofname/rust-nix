{
    pkgs,
}:
{
    libgit2-sys.override = {
        extraBuildInputs = [ pkgs.cmake pkgs.zlib ];
    };
    libz-sys.override = {
        extraBuildInputs = [ pkgs.zlib ];
    };
    curl-sys.override = {
        extraBuildInputs = [ pkgs.buildPackages.pkg-config pkgs.openssl pkgs.zlib ];
    };

    sdl2-sys.override = {
        extraBuildInputs = [ pkgs.SDL2 pkgs.buildPackages.pkg-config ];
    };

    lorri.override = {
        BUILD_REV_COUNT = 1;
    };

    openssl-sys.override = {
        extraBuildInputs = [ pkgs.buildPackages.pkg-config pkgs.openssl ];
    };
    libssh2-sys.override = {
        extraBuildInputs = [ pkgs.buildPackages.pkg-config pkgs.openssl pkgs.zlib ];
    };

    override = {
        debugInfo = 0;
        optLevel = 3;
    };
}