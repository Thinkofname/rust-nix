{
    pkgs
}:
{
    optional = cond: val: if cond then
        val
    else null;
    optionalList = pkgs.lib.lists.optionals;
    optionalString = pkgs.lib.strings.optionalString;
}