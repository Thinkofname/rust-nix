{
    pkgs,
    index
}:
let
    # rustSrc = pkgs.fetchFromGitHub {
    #     owner = "rust-lang";
    #     repo = "rust";
    #     rev = "3c235d5600393dfe6c36eeed34042efad8d4f26e";
    #     sha256 = "0fmk433yxqaqb7n3h751k2g72wm916bb0zwc8gqqd9dy0qazn4i7";
    #     fetchSubmodules = true;
    # };
    rustSrc = builtins.fetchTarball {
        url = "https://static.rust-lang.org/dist/rustc-1.35.0-src.tar.gz";
        sha256 = "0bm09yn5yl6xdr3dym27rcfxjsjdb98py0fjg35dbyb6w1i2g6ml";
    };
    # TODO: Bootstrap correctly instead of this
    stage0 = (pkgs.rustChannelOf { date = "2019-05-14"; channel = "1.34.2"; }).rust;
    stage1 = buildRustc {
        bootstrap_rustc = stage0;
        isStage0 = true;
    };
    stage2 = buildRustc {
        bootstrap_rustc = stage1;
    };

    buildRustc = {
        bootstrap_rustc,
        isStage0 ? false,
    }: let
        llvmShared = pkgs.llvm_7.override { enableSharedLibraries = true; };
        configCommon = {
            rustc_llvm.override = {
                REAL_LIBRARY_PATH_VAR = "LD_LIBRARY_PATH";
                LD_LIBRARY_PATH = "";
                LLVM_LINK_SHARED = "1";
                extraBuildInputs = [ llvmShared ];
            };
            compiler_builtins.override = {
                RUST_COMPILER_RT_ROOT = pkgs.stdenv.mkDerivation {
                    name = "compiler-rt-unpacked";
                    src = pkgs.llvmPackages_7.compiler-rt.src;
                    phases = [ "unpackPhase" "buildPhase" ];
                    buildPhase = ''
                        mkdir -p $out
                        cp -r * $out/
                    '';
                };
            };
        };

        rust = sysroot: rustc: import ../. (let
            extraStageFlag = if rustc == bootstrap_rustc && isStage0 then
                " --cfg stage0"
            else "";
        in {
            inherit pkgs index;
            bootstrap_rustc = rustc;
            cratesIOPatch = {
                rustc-std-workspace-core = rustSrc + "/src/tools/rustc-std-workspace-core";
                backtrace-sys = {
                    # TODO: Hack due to missing features otherwise
                    version = "=0.1.27";
                };
            };
            sysroot = sysroot;
            extraMetadata = "-rustc";
            config = {
                rustc_codegen_llvm.override = {
                    extraRustFlags = "-Cprefer-dynamic -Z force-unstable-if-unmarked${extraStageFlag} -L ${pkgs.libxml2.out}/lib";
                    buildScriptOverride = {
                        extraRustFlags = "-Cprefer-dynamic -Z force-unstable-if-unmarked${extraStageFlag} -L ${pkgs.libxml2.out}/lib";
                    };
                };
                panic_abort.override = {
                    extraRustFlags = "-Cprefer-dynamic -C panic=abort -Z force-unstable-if-unmarked";
                    buildScriptOverride = {
                        extraRustFlags  = "-Cprefer-dynamic -Z force-unstable-if-unmarked";
                    };
                };
                override = {
                    # Allow for nightly features on stable
                    RUSTC_BOOTSTRAP = "1";
                    extraRustFlags = "-Cprefer-dynamic -Z force-unstable-if-unmarked${extraStageFlag}";
                    CFG_COMPILER_HOST_TRIPLE = pkgs.stdenv.hostPlatform.config;
                    RUSTC_ERROR_METADATA_DST = "tmp/extended-error-metadata";
                    CFG_RELEASE_CHANNEL = "stable";
                    CFG_VERSION = "1.35.0";
                    CFG_RELEASE = "1.35.0";
                    buildScriptOverride = {
                        extraRustFlags = "-Cprefer-dynamic -Z force-unstable-if-unmarked${extraStageFlag}";
                    };
                };
            } // configCommon;
            hostRust = import ../. {
                inherit pkgs index bootstrap_rustc;
                isHost = true;
                extraMetadata = "-rustc-host";
                config = {
                    override = {
                        # Allow for nightly features on stable
                        RUSTC_BOOTSTRAP = "1";
                        CFG_COMPILER_HOST_TRIPLE = pkgs.stdenv.hostPlatform.config;
                        RUSTC_ERROR_METADATA_DST = "tmp/extended-error-metadata";
                        CFG_RELEASE_CHANNEL = "stable";
                        CFG_VERSION = "1.35.0";
                        CFG_RELEASE = "1.35.0";
                        extraRustFlags = "--cfg stage0";
                    };
                } // configCommon;
            };
        });
        std = (rust "/none" bootstrap_rustc).buildCargo {
            src = rustSrc + "/src/libstd";
            features = [
                "panic-unwind"
                "backtrace"
            ];
        };
        sysroot-std = pkgs.stdenv.mkDerivation {
            name = "sysroot-std";

            phases = [ "buildPhase" ];
            buildPhase = with pkgs.buildPackages; ''
                ${coreutils}/bin/mkdir -p $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib
                ${coreutils}/bin/cp -r ${std}/lib/* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib
                ${
                    builtins.concatStringsSep "\n" (map
                        (x: "${coreutils}/bin/cp ${x}* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib")
                        std.crateSearchPaths)
                }
            '';
        };
        test = (rust sysroot-std bootstrap_rustc).buildCargo {
            src = rustSrc + "/src/libtest";
        };
        test-std = pkgs.stdenv.mkDerivation {
            name = "test-std";

            phases = [ "buildPhase" ];
            buildPhase = with pkgs.buildPackages; ''
                ${coreutils}/bin/mkdir -p $out
                ${coreutils}/bin/cp -r --no-preserve=all ${sysroot-std}/* $out/
                ${coreutils}/bin/cp -r ${test}/lib/* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib

                ${
                    builtins.concatStringsSep "\n" (map
                        (x: "${coreutils}/bin/cp --no-preserve=all ${x}* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib")
                        test.crateSearchPaths)
                }
            '';
        };
        rustc = (rust test-std bootstrap_rustc).buildCargo {
            src = rustSrc + "/src/rustc";
        };
        sysroot-rustc = pkgs.stdenv.mkDerivation {
            name = "sysroot-rustc";

            phases = [ "buildPhase" ];
            buildPhase = with pkgs.buildPackages; ''
                ${coreutils}/bin/mkdir -p $out
                ${coreutils}/bin/cp -r --no-preserve=all ${test-std}/* $out/

                ${coreutils}/bin/mkdir -p $out/bin
                ${coreutils}/bin/cp --no-preserve=all ${rustc}/bin/rustc_binary $out/bin/rustc

                ${
                    builtins.concatStringsSep "\n" (map
                        (x: "${coreutils}/bin/cp --no-preserve=all ${x}* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib")
                        rustc.crateSearchPaths)
                }
            '';
        };
        codegen_llvm = (rust sysroot-rustc bootstrap_rustc).buildCargo {
            src = rustSrc + "/src/librustc_codegen_llvm";
        };

        # Now to recompile std with our new compiler
        rustc_incomplete = pkgs.stdenv.mkDerivation {
            name = "rustc";

            phases = [ "buildPhase" ];
            buildPhase = with pkgs.buildPackages; ''
                ${coreutils}/bin/mkdir -p $out/bin
                ${coreutils}/bin/cp --no-preserve=all -r ${rustc}/bin/rustc_binary $out/bin/rustc
                ${coreutils}/bin/chmod +x $out/bin/*

                ${coreutils}/bin/mkdir -p $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/codegen-backends/
                ${coreutils}/bin/cp --no-preserve=all -r ${codegen_llvm}/lib/librustc_codegen_llvm*.so $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/codegen-backends/librustc_codegen_llvm-llvm.so
            '';
        };

        std_new = (rust rustc_incomplete rustc_incomplete).buildCargo {
            src = rustSrc + "/src/libstd";
            features = [
                "panic-unwind"
                "backtrace"
            ];
        };
        sysroot-std_new = pkgs.stdenv.mkDerivation {
            name = "sysroot-std_new";

            phases = [ "buildPhase" ];
            buildPhase = with pkgs.buildPackages; ''
                ${coreutils}/bin/mkdir -p $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib
                ${coreutils}/bin/cp -r ${std_new}/lib/* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib
                ${
                    builtins.concatStringsSep "\n" (map
                        (x: "${coreutils}/bin/cp ${x}* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib")
                        std_new.crateSearchPaths)
                }
            '';
        };
        test_new = (rust sysroot-std_new rustc_incomplete).buildCargo {
            src = rustSrc + "/src/libtest";
        };
        test-std_new = pkgs.stdenv.mkDerivation {
            name = "test-std";

            phases = [ "buildPhase" ];
            buildPhase = with pkgs.buildPackages; ''
                ${coreutils}/bin/mkdir -p $out
                ${coreutils}/bin/cp -r --no-preserve=all ${sysroot-std_new}/* $out/
                ${coreutils}/bin/cp -r ${test_new}/lib/* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib

                ${
                    builtins.concatStringsSep "\n" (map
                        (x: "${coreutils}/bin/cp --no-preserve=all ${x}* $out/lib/rustlib/${pkgs.stdenv.hostPlatform.config}/lib")
                        test_new.crateSearchPaths)
                }
            '';
        };

    in pkgs.stdenv.mkDerivation {
        name = "rustc";

        phases = [ "buildPhase" ];
        buildPhase = with pkgs.buildPackages; ''
            ${coreutils}/bin/mkdir -p $out
            ${coreutils}/bin/cp --no-preserve=all -r ${rustc_incomplete}/* $out/
            ${coreutils}/bin/chmod +x $out/bin/*

            ${coreutils}/bin/cp --no-preserve=all -r ${test-std_new}/* $out/
        '';
    };
in stage1