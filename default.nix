{
    pkgs,
    index,
    cratesIOPatch ? {},
    bootstrap_rustc ? null,
    config ? {},
    preferToml ? false,
    sysroot ? null,
    extraMetadata ? "",
    hostRust ? null,
    isHost ? false,
}:
let
    hostRust_ = hostRust;
in let
    sysroot_ = sysroot;

    rustc = if bootstrap_rustc == null then
        import ./rustc { inherit pkgs index; }
    else bootstrap_rustc;

    hostRust = if isHost then
        abort "Shouldn't use host rust when already a host"
    else if hostRust_ == null then
        # TODO: This isn't great for buildscripts due to the target env vars
        # being wrong when cross compiling
        import ./. {
            inherit index cratesIOPatch config preferToml sysroot extraMetadata;
            pkgs = pkgs.buildPackages;
            bootstrap_rustc = rustc;
            isHost = true;
        }
    else hostRust_;

    parse = import ./parse.nix {lib = pkgs.lib;};
    toml = import ./toml.nix { inherit pkgs parse; };
    semver = import ./semver.nix { lib = pkgs.lib; inherit parse; };
    cfg = import ./cfg.nix { lib = pkgs.lib; inherit parse; };
    cratesio = import ./crates-io.nix { inherit pkgs index semver; patch = cratesIOPatch; };
    crateConfig = pkgs.lib.attrsets.recursiveUpdate (import ./config.nix { inherit pkgs; }) config;
    util = import ./util.nix { inherit pkgs; };

    # Run rustc to fetch the cfg params for the current/target
    # system.
    rustcCfgParams = import ./cfgParams.nix {
        inherit pkgs parse;
        platform = pkgs.stdenv.hostPlatform;
    };
    rustcBuildCfgParams = import ./cfgParams.nix {
        inherit pkgs parse;
        platform = pkgs.stdenv.buildPlatform;
    };

    # Returns the crate specific override config for the given
    # crate.
    #
    # This works recursively so that
    #
    #    libz-sys.override = {
    #        extraBuildInputs = [ pkgs.zlib ];
    #    };
    #
    # Applies that override to every instance of that crate whilst:
    #
    #    libz-sys."1".override = {
    #        extraBuildInputs = [ pkgs.zlib ];
    #    };
    #
    # Will apply that to only apply that to crates with a version
    # of "1.*"
    getCrateConfig = name: version:
        configRecurse crateConfig ([name] ++ (builtins.split "." version));
    configRecurse = obj: keys:
        (if obj ? override then
            obj.override
        else {}) // (if keys == [] || !(obj ? ${builtins.head keys}) then {} else
            configRecurse (obj.${builtins.head keys}) (builtins.tail keys)
        );

    # Finds all possible dependencies from a given Cargo.toml
    collectDeps = cfgParams: cargoFile: crateFeatures:
        (util.optionalList (cargoFile ? dependencies) (collectDepsImpl cargoFile.dependencies crateFeatures))
            ++ (if cargoFile ? target then
                builtins.foldl' (a: target:
                let
                    parsed = cfg.parse target;
                in
                    if parsed != null then
                        if cfg.eval cfgParams parsed.value then
                            a ++ (collectDepsImpl (cargoFile.target.${target}.dependencies) crateFeatures)
                        else
                            a
                    else
                        a
                ) [] (builtins.attrNames cargoFile.target)
            else []);

    collectDepsImpl = dependencies: crateFeatures:
        (builtins.filter (x:
            if x ? optional && x.optional then
                (builtins.any (f: builtins.elemAt f 0 == x.name) (map (x: builtins.split "/" x) crateFeatures))
            else true
        )
        (map (x: let
                dep = dependencies.${x};
            in if builtins.isString dep then
                {name = x; version = dep;}
            else
                dep // {name = x;}
            ) (builtins.attrNames dependencies)));

    collectDepsIndex = cfgParams: index: crateFeatures:
        collectDepsIndexImpl (builtins.filter (x:
            let
                parsed = cfg.parse x.target;
            in
                if x.kind != "normal" && x.kind != "build" then
                    false
                else if x.target == null then
                    true
                else if parsed != null then
                    if cfg.eval cfgParams parsed.value then
                        true
                    else
                        false
                else
                    false
        ) index.deps) crateFeatures;

    collectDepsIndexImpl = dependencies: crateFeatures:
        builtins.filter (x:
            if x ? optional && x.optional then
                (builtins.any (f: builtins.elemAt f 0 == x.name) (map (x: builtins.split "/" x) crateFeatures))
            else true
        ) dependencies;

    # Removes duplicate values from a list
    #
    # Doesn't keep the order
    removeDuplicateValues = list:
        builtins.attrNames (builtins.foldl' (cur: f: cur // {${f} = true;}) {} list);
    mergeFeatures = a: b:
        let
            base = a // b;
            merged = builtins.foldl' (cur: key:
                if b ? ${key} then
                    cur // {${key} = removeDuplicateValues (a.${key} ++ b.${key});}
                else
                    cur
            ) base (builtins.attrNames a);
        in
            merged
        ;

    # Parses features from a Cargo.toml
    cargoFeaturesToml = {
        src,
        features? ["default"],
        handledCrates ? {},
        collectedDeps ? null,
    }: let
        cargoFile = toml.fromTOML (builtins.readFile "${src}/Cargo.toml");

        collectFeatures = key: if cargoFile.features ? ${key} then
             (builtins.foldl' (a: f: a ++ collectFeatures f) [key] cargoFile.features.${key})
        else [key];
        crateFeatures = if cargoFile ? features then
             (builtins.foldl' (a: f: a ++ collectFeatures f) [] features)
        else features;

        deps = if collectedDeps != null then
            collectedDeps
        else collectDeps rustcCfgParams cargoFile crateFeatures;

        info = (builtins.foldl' (state: x: let
            features = (if x ? "default-features" && !x.default-features then [] else ["default"])
                ++ (map (x: builtins.elemAt x 2)
                        (builtins.filter (f: (builtins.elemAt f 0) == x.name && builtins.length f == 3)
                            (map (x: builtins.split "/" x) crateFeatures)))
                ++ (util.optionalList (x ? features) x.features);
            info = if x ? version && !preferToml then
                let
                    index = cratesio.findCrate {
                        name = if x ? package then x.package else x.name;
                        version = x.version;
                    };
                in if state.handled ? "${index.name}-${index.vers} ${builtins.toString features}" then
                    {
                        features = {};
                        handled = state.handled;
                    }
                else
                    cargoFeaturesIndexImpl {
                        name = if x ? package then x.package else x.name;
                        version = x.version;
                        inherit features;
                        handledCrates = state.handled;
                        existingIndex = index;
                    }
            else if state.handled ? "${x.name} toml ${builtins.toString features}" then
                    {
                        features = {};
                        handled = state.handled;
                    }
                else cargoFeaturesToml {
                    src = if x ? version then
                            cratesio.crate {
                                name = if x ? package then x.package else x.name;
                                version = x.version;
                            }
                        else if x ? path then
                            src + ("/" + x.path)
                        else if x ? git then
                            builtins.fetchGit {
                                url = x.git;
                                rev = x.rev;
                            }
                        else abort("Can't handle dep: ${parse.debugPrint x}");
                    inherit features;
                    handledCrates = state.handled;
                };
        in {
            features = state.features ++ [info.features];
            handled = info.handled;
        }) {
            features = [];
            handled = handledCrates;
        } deps);

    in {
        features = builtins.foldl' mergeFeatures ({
            "${cargoFile.package.name}-${cargoFile.package.version}" = removeDuplicateValues (map (x: builtins.elemAt (builtins.split "/" x) 0) crateFeatures);
        }) info.features;
        handled = info.handled // {
            "${cargoFile.package.name} toml ${builtins.toString features}" = true;
        };
    };

    # Parses features from the crate index
    #
    # Whilst this can be done via the toml file this allows us to
    # save downloading the crates until a later stage.
    cargoFeaturesIndex = {
        name,
        version,
        features? ["default"],
    }: (cargoFeaturesIndexImpl {
        inherit name version features;
    }).features;

    cargoFeaturesIndexImpl = {
        name,
        version,
        features? ["default"],
        handledCrates? {},
        existingIndex ? null,
    }: let
        index = if existingIndex != null then existingIndex else cratesio.findCrate {
            inherit name version;
        };

        deps = collectDepsIndex rustcCfgParams index crateFeatures;
        depsFull = map (x: {
            index = cratesio.findCrate {
                name = if x ? package then x.package else x.name;
                version = x.req;
            };
            dep = x;
        }) deps;

        handledNew = handledCrates // {"${name}-${index.vers} ${builtins.toString features}" = true;};

        collectFeatures = key: if index.features ? ${key} then
            (builtins.foldl' (a: f: a ++ collectFeatures f) [key] index.features.${key})
        else [key];
        crateFeatures = if index ? features then
            (builtins.foldl' (a: f: a ++ collectFeatures f) [] features)
        else features;

        info = builtins.foldl' (state: x: let
            features = (if x.dep ? "default_features" && !x.dep.default_features then [] else ["default"])
                ++ (map (x: builtins.elemAt x 2)
                        (builtins.filter (f: (builtins.elemAt f 0) == x.dep.name && builtins.length f == 3)
                            (map (x: builtins.split "/" x) crateFeatures)))
                ++ x.dep.features;
            cfi = cargoFeaturesIndexImpl {
                name = x.dep.name;
                version = x.dep.req;
                inherit features;
                handledCrates = state.handled;
                existingIndex = x.index;
            };
        in if state.handled ? "${x.index.name}-${x.index.vers} ${builtins.toString features}" then state else {
            features = state.features ++ [cfi.features];
            handled = cfi.handled;
        }) {
            features = [];
            handled = handledNew;
        } depsFull;

    in {
        features = builtins.foldl' mergeFeatures ({
            "${name}-${index.vers}" = removeDuplicateValues (map (x: builtins.elemAt (builtins.split "/" x) 0) crateFeatures);
        }) info.features;
        handled = info.handled;
    };

    # Builds a crate from crates.io
    crate = args:
        (crate' args).result;
    crate' = {
        name,
        version,
        features? ["default"],
        globalFeatures ? null,
        build ? "all",
        crates ? {},
        sysroot ? sysroot_,
    }:  let
            result = buildCargo' {
                inherit features globalFeatures build crates sysroot;
                src = cratesio.crate {
                    inherit name version;
                };
            };
        in result;

    parseBuildScript = with builtins; buildScript:
        let
            stdout = filter isString (split "\n" (readFile "${buildScript}/stdout"));

            rustc_cfg = "cargo:rustc-cfg=";
            rustc_env = "cargo:rustc-env=";
            rustc_link_lib = "cargo:rustc-link-lib=";
            rustc_link_search = "cargo:rustc-link-search=";
            rustc_flags = "cargo:rustc-flags=";
        in
        {
            features = map
                (x: substring (stringLength rustc_cfg) (stringLength x) x)
                (filter (x: substring 0 (stringLength rustc_cfg) x == rustc_cfg) stdout);
            env = map
                (x: substring (stringLength rustc_env) (stringLength x) x)
                (filter (x: substring 0 (stringLength rustc_env) x == rustc_env) stdout);
            linkLib = map
                (x: substring (stringLength rustc_link_lib) (stringLength x) x)
                (filter (x: substring 0 (stringLength rustc_link_lib) x == rustc_link_lib) stdout);
            linkSearch = map
                (x: substring (stringLength rustc_link_search) (stringLength x) x)
                (filter (x: substring 0 (stringLength rustc_link_search) x == rustc_link_search) stdout);
            flags = map
                (x: substring (stringLength rustc_flags) (stringLength x) x)
                (filter (x: substring 0 (stringLength rustc_flags) x == rustc_flags) stdout);
        };

    runBuildScript = {
        name,
        version,
        src,
        buildScript,
        rustDeps,
        features,
        edition,
    }: let
        overrides_ = getCrateConfig name version;
        overrides = builtins.removeAttrs
            (overrides_ // (if overrides_ ? buildScriptOverride then overrides_.buildScriptOverride else {}))
            [ "buildScriptOverride" ];
        extraBuildInputs = util.optionalList (overrides ? extraBuildInputs) overrides.extraBuildInputs;
    in pkgs.stdenv.mkDerivation ({
        name = "${name}-${version}-build-script";
        src = buildRust {
            name = name;
            isBuildScript = true;
            inherit version src features rustDeps edition;
            target = pkgs.stdenv.buildPlatform.config;
            bins = [{
                name = "build_script";
                path = buildScript;
            }];
            sysroot = null;
        };

        buildInputs = extraBuildInputs;

        phases = [ "buildPhase" ];
        buildPhase = ''
export RUSTC=${rustc}/bin/rustc

export CARGO_PKG_NAME=${name}
export CARGO_PKG_VERSION=${version}
export CARGO_MANIFEST_DIR=${src}

${with builtins;
    foldl' (base: cfg: let
            param = rustcBuildCfgParams.${cfg};
        in
            base + (if isList param && length param == 1 then
                "export CARGO_CFG_${pkgs.lib.strings.toUpper cfg}=${elemAt param 0}"
            else
                "export CARGO_CFG_${pkgs.lib.strings.toUpper cfg}=1")
    ) "" (attrNames rustcBuildCfgParams)
}

export OPT_LEVEL=${toString overrides.optLevel}
export DEBUG=${if overrides.debugInfo!=0 then "true" else "false"}
export TARGET=${pkgs.stdenv.hostPlatform.config}
export HOST=${pkgs.stdenv.buildPlatform.config}
export PROFILE=${if overrides.optLevel==0 then "debug" else "release"}
export RUST_BACKTRACE=full

${pkgs.buildPackages.coreutils}/bin/mkdir -p $out/out_files
export OUT_DIR=$out/out_files;
(cd ${src} && $src/bin/build_script) | tee $out/stdout
        '';
    } // overrides);

    doBuildScript = {
        cargoFile,
        src,
        buildScript,
        crateFeatures,
    }: let
        collectedDeps = if cargoFile ? "build-dependencies" then
            (collectDepsImpl cargoFile."build-dependencies" crateFeatures)
        else [];

        globalFeatures = (cargoFeaturesToml {
            inherit src;
            features = crateFeatures;
            collectedDeps = collectedDeps;
        }).features;
    in runBuildScript {
        name = cargoFile.package.name;
        version = cargoFile.package.version;
        src = src;
        buildScript = buildScript;
        rustDeps = map (x:
                if x ? version then
                    crate {
                        name = if x ? package then x.package else x.name;
                        version = x.version;
                        build = "lib";
                        sysroot = null;
                        globalFeatures = globalFeatures;
                    }
                else if x ? path then
                    buildCargo {
                        src = src + ("/" + x.path);
                        build = "lib";
                        sysroot = null;
                        globalFeatures = globalFeatures;
                    }
                else if x ? git then
                    buildCargo {
                        src = builtins.fetchGit {
                            url = x.git;
                            rev = x.rev;
                        };
                        build = "lib";
                        sysroot = null;
                        globalFeatures = globalFeatures;
                    }
                else abort("Can't handle dep: ${parse.debugPrint x}"))
            collectedDeps;
        features = crateFeatures;

        edition = if cargoFile.package ? edition then
                cargoFile.package.edition
            else
                "2015";
    };

    # Builds a rust crate from a Cargo.toml
    buildCargo = args:
        (buildCargo' args).result;

    buildCargo' = {
        src,
        features? ["default"],
        globalFeatures ? null,
        build ? "all",
        crates ? {},
        sysroot ? sysroot_,
    }: let
        cargoFile = toml.fromTOML (builtins.readFile "${src}/Cargo.toml");
        hasSrc = (builtins.readDir src) ? src;

        globalFeaturesMap = if globalFeatures != null then
            globalFeatures
        else
            (cargoFeaturesToml { inherit src features; }).features
        ;

        crateFeatures = globalFeaturesMap."${cargoFile.package.name}-${cargoFile.package.version}";

        allowBuildScript = (cargoFile.package ? build && builtins.isBool cargoFile.package.build && cargoFile.package.build)
            || !(cargoFile.package ? build);

        buildScript = if cargoFile.package ? build && builtins.isString cargoFile.package.build then
            "/" + cargoFile.package.build
        else if (builtins.readDir (src)) ? "build.rs" && allowBuildScript then
            "/build.rs"
        else null;

        buildScriptOutput = if buildScript != null then
            (if isHost then doBuildScript else hostRust.doBuildScript) {
                inherit cargoFile src buildScript crateFeatures;
            }
        else null;

        collectedDeps = collectDeps rustcCfgParams cargoFile crateFeatures;

        deps = builtins.foldl' (state: x: let
            c = if x ? version then
                    crate' {
                        name = if x ? package then x.package else x.name;
                        version = x.version;
                        globalFeatures = globalFeaturesMap;
                        build = "lib";
                        crates = state.crates;
                        inherit sysroot;
                    }
                else if x ? path then
                    buildCargo' {
                        src = src + ("/" + x.path);
                        globalFeatures = globalFeaturesMap;
                        build = "lib";
                        crates = state.crates;
                        inherit sysroot;
                    }
                else if x ? git then
                    buildCargo' {
                        src = builtins.fetchGit {
                            url = x.git;
                            rev = x.rev;
                        };
                        globalFeatures = globalFeaturesMap;
                        build = "lib";
                        crates = state.crates;
                        inherit sysroot;
                    }
                else abort("Can't handle dep: ${parse.debugPrint x}");
            in {
                deps = state.deps ++ [c.result];
                crates = c.crates;
            })
            {
                deps = [];
                crates = crates;
            }
            collectedDeps;

        renamedDeps = builtins.foldl' (a: b: a // b) {}
            (map (x: { ${builtins.replaceStrings ["-"] ["_"] x.package} = builtins.replaceStrings ["-"] ["_"] x.name; }) (builtins.filter (x: x ? package) collectedDeps));

        isProcMacro = if cargoFile ? lib && cargoFile.lib ? proc-macro then cargoFile.lib.proc-macro else false;

        result = if isProcMacro && !isHost then
            hostRust.buildCargo {
                inherit src features build;
            }
        else buildRust {
            name = cargoFile.package.name;
            version = cargoFile.package.version;
            src = src;
            lib = if build != "lib" && build != "all" then
                null
            else if cargoFile ? lib then
                {
                    name = builtins.replaceStrings ["-"] ["_"] (if cargoFile.lib ? name then
                        cargoFile.lib.name
                    else cargoFile.package.name);
                    path = if cargoFile.lib ? path then
                        "/" + cargoFile.lib.path
                    else "/src/lib.rs";
                    proc-macro = isProcMacro;
                    crate-type = if cargoFile.lib ? crate-type then cargoFile.lib.crate-type else [ "rlib" ];
                }
            else if hasSrc && (builtins.readDir (src + "/src")) ? "lib.rs" then
                {
                    name = builtins.replaceStrings ["-"] ["_"] cargoFile.package.name;
                    path = "/src/lib.rs";
                    proc-macro = false;
                    crate-type = [ "rlib" ];
                }
            else
                null;
            buildScript = buildScriptOutput;

            edition = if cargoFile.package ? edition then
                    cargoFile.package.edition
                else
                    "2015";
            features = crateFeatures;

            bins = if build != "bin" && build != "all" then
                []
            else if cargoFile ? bin then
                map (x: {
                    name = builtins.replaceStrings ["-"] ["_"] x.name;
                    path = "/" + x.path;
                }) cargoFile.bin
            else if hasSrc && (builtins.readDir (src + "/src")) ? "main.rs" then
                [{
                    name = builtins.replaceStrings ["-"] ["_"] cargoFile.package.name;
                    path = "/src/main.rs";
                }]
            else
                [];

            rustDeps = deps.deps;
            renamedDeps = renamedDeps;
            sysroot = sysroot;
        };
        key = "${cargoFile.package.name}-${cargoFile.package.version}";
    in if builtins.hasAttr key crates then
        {
            result = builtins.getAttr key crates;
            crates = crates;
        }
    else {
        result = result;
        crates = deps.crates // {
            ${key} = result;
        };
    };
    # Builds a rust crate
    buildRust = {
        name,
        version,
        src,
        lib ? null,
        bins ? [],
        rustDeps ? [],
        renamedDeps ? {},
        features ? ["default"],
        edition ? "2015",
        buildScript ? null,
        isBuildScript ? false,
        override ? {},
        target ? pkgs.stdenv.hostPlatform.config,
        sysroot ? sysroot_,
    }: let
        libExtension = ty: {
            rlib = "rlib";
            dylib = "so";
        }.${ty};

        depString = builtins.foldl' (a: dep: a + (let
            normName = builtins.replaceStrings ["-"] ["_"] dep.crateLib.name;
            trueName = if builtins.hasAttr normName renamedDeps then
                builtins.getAttr normName renamedDeps
                else
                    dep.crateLib.name
            ;
        in if dep.crateLib.proc-macro then
            "--extern ${trueName}=${dep}/lib/lib${normName}-${dep.nameHash}.so "
            else
            "--extern ${trueName}=${dep}/lib/lib${normName}-${dep.nameHash}.${libExtension (builtins.head dep.crateLib.crate-type)} "
        )) "" (pkgs.lib.lists.unique rustDeps);
        crateSearchPaths = pkgs.lib.lists.unique ((builtins.foldl'
                (a: dep: a ++ ["${dep}/lib/"] ++ dep.crateSearchPaths) []
                rustDeps)
                ++ (util.optionalList (overrides ? extraSearchPaths) overrides.extraSearchPaths));

        buildScriptInfo = util.optional (buildScript != null) (parseBuildScript buildScript);
        buildScriptArgs = util.optionalString (buildScript != null)
            ((builtins.foldl' (a: b: a + " -l ${b}") "" buildScriptInfo.linkLib)
                + (builtins.foldl' (a: b: a +  " -L ${b}") "" buildScriptInfo.linkSearch));

        buildScriptCfg = removeDuplicateValues ((util.optionalList
            (buildScript != null)
            (map (pkgs.lib.strings.escape [''"'']) buildScriptInfo.features))
            ++ (builtins.foldl'
                (a: dep: a ++ dep.buildScriptCfg) []
                rustDeps));
        featuresString = builtins.foldl' (a: f: a + "--cfg ${f} ") "" (
            builtins.filter (x: (builtins.match ".*/.*" x) == null) (
                (map (f: "feature=\\\"${pkgs.lib.strings.escape [''"''] f}\\\"") features)
                    ++ buildScriptCfg));

        overrides_ = (getCrateConfig name version) // override;
        overrides = builtins.removeAttrs
            (overrides_ // (if overrides_ ? buildScriptOverride && isBuildScript then overrides_.buildScriptOverride else {}))
            [ "buildScriptOverride" ];

        extraBuildInputs = (util.optionalList (overrides ? extraBuildInputs) overrides.extraBuildInputs)
            ++ (builtins.foldl' (a: dep: a ++ dep.extraBuildInputs) [] rustDeps);

        crateSearchPathsString = builtins.foldl' (a: dep: a + "-L dependency=${dep} ") "" crateSearchPaths;

        extraFlags = (util.optionalList (buildScript != null) buildScriptInfo.flags)
            ++ (builtins.foldl'
                (a: dep: a ++ dep.extraFlags) []
                rustDeps);

        hash = builtins.hashString "sha256" "${name}-${version} ${builtins.toString features} ${builtins.toString sysroot}";
        nameHash = builtins.substring 0 8 hash;
    in pkgs.stdenv.mkDerivation ({
        name = "${name}-${version}" + (if isBuildScript then "-build-script" else "");
        src = src;

        passthru = {
            crateLib = lib;
            nameHash = nameHash;
            inherit extraBuildInputs crateSearchPaths buildScriptCfg extraFlags;
        };
        buildInputs = extraBuildInputs;

        debugInfo = 2;
        optLevel = 0;

        extraRustFlags = "";

        phases = [ "buildPhase" "fixupPhase" ];
        stripDebugList = [ "bin" ];

        buildPhase = ''
export RUSTC=${rustc}/bin/rustc

export CARGO_PKG_NAME=${name}
export CARGO_PKG_VERSION=${version}

${with builtins;
    foldl' (base: cfg: let
            param = rustcBuildCfgParams.${cfg};
        in
            base + (if isList param && length param == 1 then
                "export CARGO_CFG_${pkgs.lib.strings.toUpper cfg}=${elemAt param 0}"
            else
                "export CARGO_CFG_${pkgs.lib.strings.toUpper cfg}=1")
    ) "" (attrNames rustcBuildCfgParams)
}

export OPT_LEVEL=${toString overrides.optLevel}
export DEBUG=${if overrides.debugInfo!=0 then "true" else "false"}
export TARGET=${pkgs.stdenv.hostPlatform.config}
export HOST=${pkgs.stdenv.buildPlatform.config}
export PROFILE=${if overrides.optLevel==0 then "debug" else "release"}

# TODO: Placeholders
export CARGO_PKG_AUTHORS=NYI

function rustc {
    echo "running ${rustc}/bin/rustc $@"
    ${rustc}/bin/rustc $@
}

${util.optionalString (buildScript != null) "export OUT_DIR=${buildScript}/out_files"}
${util.optionalString (buildScriptInfo != null) (builtins.foldl' (a: b: a + "export ${b}\n") "" buildScriptInfo.env)}
    ''
        # Build the main lib first
            + (if lib != null then ''
${pkgs.buildPackages.coreutils}/bin/mkdir -p $out/lib
rustc --crate-name ${lib.name} $src${lib.path} \
    --target ${target} \
    ${
        if lib.proc-macro then
            "--crate-type proc-macro"
        else
            builtins.toString (map (x: "--crate-type ${x} ") lib.crate-type)
    } \
    --edition=${edition} \
    --cap-lints allow \
    -C debuginfo=$debugInfo \
    -C opt-level=$optLevel \
    -C metadata=${hash}${extraMetadata} \
    -C extra-filename=-${nameHash} \
    --remap-path-prefix "$src=/build/${name}-${version}" \
    ${util.optionalString (sysroot != null) "--sysroot ${sysroot}"} \
    --out-dir $out/lib ${depString} ${crateSearchPathsString} ${featuresString} ${buildScriptArgs} ${builtins.concatStringsSep "" extraFlags} $extraRustFlags
        '' else "")
        # Build any binaries now
            + builtins.foldl' (a: bin: a + ''
${pkgs.buildPackages.coreutils}/bin/mkdir -p $out/bin
rustc --crate-name ${bin.name} $src${bin.path} \
    --target ${target} \
    --crate-type bin \
    --edition=${edition} \
    --cap-lints allow \
    -C debuginfo=$debugInfo \
    -C opt-level=$optLevel \
    --remap-path-prefix "$src=/build/${name}-${version}" \
    ${util.optionalString (sysroot != null) "--sysroot ${sysroot}"} \
    --out-dir $out/bin ${depString} ${crateSearchPathsString} ${featuresString} ${buildScriptArgs} ${builtins.concatStringsSep "" extraFlags} \
    ${util.optionalString (lib != null) "--extern ${lib.name}=$out/lib/lib${lib.name}-${nameHash}.rlib"} $extraRustFlags

        '') "" bins
        ;
    } // overrides);
in
{
    inherit crate buildCargo buildRust rustc doBuildScript;
}