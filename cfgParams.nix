{
    pkgs,
    parse ? import ./parse.nix {lib = pkgs.lib;},
    platform
}:
with builtins;
with parse;
let
    config = pkgs.stdenv.mkDerivation {
        name = "rustc-cfg-${platform.config}";

        debugInfo = 2;
        optLevel = 0;

        phases = [ "buildPhase" ];
        buildPhase = ''
${pkgs.buildPackages.rustc}/bin/rustc --target=${platform.config} --print cfg > $out
        '';
    };
    ident = input:
        let
            nameParse = takeWhile
                (char (x: (match "[a-zA-Z0-9_\\.]" x) != null || x == "-"))
                input;
            name = if nameParse.value != [] then
                {
                    value = concatStringsSep "" nameParse.value;
                    input = nameParse.input;
                }
            else
                null;
        in
            name
        ;
    valueString = input:
        let
            start = char (x: x == "\"") input;
            str = if start != null then escapedString "\"" start.input else null;
            end = if str != null then
                    char (x: x == "\"") str.input
                else null;
        in
            if end != null then
                {
                    value = str.value;
                    input = end.input;
                }
            else
                null;

    keyValue = input:
        let
            key = ident input;
            parsedValue = if key != null then
                andThen
                    (andThen
                        (andThen skipEmpty (char (x: x == "=")))
                        skipEmpty)
                    valueString
                    key.input
            else null;
        in
        if parsedValue != null then
        {
            value = {
                key = key.value;
                value = parsedValue.value;
            };
            input = parsedValue.input;
        } else null;

in foldl'
    (base: val:
        let
            pair = keyValue val;
        in if pair == null then
            base // { ${val} = true; }
        else if base ? "${pair.value.key}" then
            base // { ${pair.value.key} = base.${pair.value.key} ++ [pair.value.value]; }
        else
            base // { ${pair.value.key} = [pair.value.value]; }
    )
    {}
    (filter isString (split "\n" (readFile "${config}")))
