let
    moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
in {
    pkgs ? import <nixpkgs> {
        overlays = [ moz_overlay ];
    },
    rust ? import ../. {
        pkgs=pkgs;
        index = pkgs.fetchFromGitHub {
            owner = "rust-lang";
            repo = "crates.io-index";
            rev = "f6dc0132a563d0d555ce3665d8fd037d4f309dbd";
            sha256 = "0rxjf3vgaw8ivvkblxabaw5sinxg9gkkijmapwv3r8dzsmkrdzdy";
        };
    },
    name,
}:
{
    rustc = rust.rustc;
    crate = rust.crate {
        inherit name;
        version = "*";
    };
}