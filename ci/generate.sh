crates=`curl "https://crates.io/api/v1/crates?page=1&per_page=100&sort=downloads" | jq --raw-output ".crates[] | .name"`

rm .gitlab-ci.yml

cat ./ci/.gitlab-ci.yml.template > .gitlab-ci.yml

for crate in $crates; do
    cat <<EOF >> .gitlab-ci.yml

crate_$crate:
    stage: crate
    variables:
        CRATE_NAME: $crate
    script:
        - mkdir -p ~/.config/nix/
        - echo "\$CONFIG_NIX" > ~/.config/nix/nix.conf
        - mkdir /keys/
        - echo "\$SIGNING_KEY" > /keys/secret
        - nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
        - nix-channel --update
        - nix-env -i git
        - nix-build ci/doBuild.nix --no-out-link --argstr name $crate -A crate
        - nix copy --to 's3://nixcache?endpoint=buckets.thinkof.name' --all

EOF
done
