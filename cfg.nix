{
    lib ? (import <nixpkgs> {}).lib,
    parse ? import ./parse.nix {lib = lib;},
}:
with builtins;
with parse;

let
    notNull = check: expr:
        if check != null then
            expr
        else
            null;

    parse = input:
        call input;

    call = input:
        let
            name = ident input;
            bracket = notNull name (andThen skipEmpty (word "(") name.input);
            args = notNull bracket (andThen skipEmpty params bracket.input);
            close_bracket = notNull args (andThen skipEmpty (word ")") args.input);
        in notNull close_bracket {
            value = {
                type = "call";
                name = name.value;
                args = args.value;
            };
            input = close_bracket.input;
        };

    compare = input:
        let
            key = ident input;
            equals = notNull key (andThen skipEmpty (word "=") key.input);
            value = notNull equals (andThen skipEmpty valueString equals.input);
        in notNull value {
            value = {
                type = "compare";
                key = key.value;
                value = value.value;
            };
            input = value.input;
        };

    valueString = input:
        let
            start = char (x: x == "\"") input;
            str = notNull start (escapedString "\"" start.input);
            end = notNull str (char (x: x == "\"") str.input);
        in notNull end {
            value = str.value;
            input = end.input;
        };

    ident = input:
        let
            v = takeWhile (char (x: (match "[a-zA-Z0-9_\\.]" x) != null || x == "-")) input;
        in notNull v {
            value = concatStringsSep "" v.value;
            input = v.input;
        };

    parseAny = choose [
        call
        compare
        ident
    ];

    params = input: let
        val = parseAny input;
        end = notNull val (andThen skipEmpty (char (x: x == ",")) val.input);
    in
        if end != null then let
            next = andThen skipEmpty params end.input;
        in
            {
                value = [val.value] ++ next.value;
                input = next.input;
            }
        else if val != null then
            {
                value = [val.value];
                input = val.input;
            }
        else
            {
                value = [];
                input = input;
            }
    ;

    eval = params: cfg:
        if isString cfg then
            params ? ${cfg}
        else if cfg.type == "call" then
            # Passthrough, does nothing
            # Should only have one param
            if cfg.name == "cfg" then
                eval params (head cfg.args)
            # Return true if any params are true
            else if cfg.name == "any" then
                any (x: eval params x) cfg.args
            # Return true if all params are true
            else if cfg.name == "all" then
                all (x: eval params x) cfg.args
            # Inverts the result
            # Should only have one param
            else if cfg.name == "not" then
                !(eval params (head cfg.args))
            else
                abort("Invalid cfg function: ${cfg.name}")
        else if cfg.type == "compare" then
            if params ? ${cfg.key} then
                any (a: a == cfg.value) params.${cfg.key}
            else
                false
        else
            abort("Invalid cfg type: ${cfg.type}")
        ;

    b = x: if x then "true" else "false";
in
{
    inherit parse eval;

    test = b (eval {
        target_arch = "x64";
    } (parse "cfg(target_os=\"windows\")").value);
}