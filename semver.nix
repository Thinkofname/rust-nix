{
    lib ? (import <nixpkgs> {}).lib,
    parse ? import ./parse.nix {lib = lib;},
}:
with builtins;
with parse;

let
    when = cond: val: if cond then val else false;

    compareVersions = a: b:
        (a.major == b.major && a.minor == b.minor && a.patch < b.patch)
        || (a.major == b.major && a.minor < b.minor)
        || (a.major < b.major)
    ;

    requirementMatches = req: ver:
        let
            moreThan =
                (when (req.version.patch != "*") (req.version.major == ver.major && req.version.minor == ver.minor && ver.patch > req.version.patch))
                || (when (req.version.minor != "*") (req.version.major == ver.major && ver.minor > req.version.minor))
                || (when (req.version.major != "*") (ver.major > req.version.major))
                || (when (req.version.major == "*") true);
            lessThan =
                (when (req.version.patch != "*") (req.version.major == ver.major && req.version.minor == ver.minor && ver.patch < req.version.patch))
                || (when (req.version.minor != "*") (req.version.major == ver.major && ver.minor < req.version.minor))
                || (when (req.version.major != "*") (ver.major < req.version.major))
                || (when (req.version.major == "*") true);
            equal = req.version.major == ver.major && req.version.minor == ver.minor && req.version.patch == ver.patch;
            valid =
                # Compatible with
                if req.prefix == "^" || req.prefix == "" then
                    if req.version.major == "*" then
                        true
                    # The 0 version is treated differently
                    else if req.version.major == 0 then
                        if req.version.minor != "*" then
                            req.version.minor == ver.minor
                        else true
                    else
                        req.version.major == ver.major
                else if req.prefix == "~" then
                    if req.version.major == "*" then
                        true
                    # The 0 version is treated differently
                    else if req.version.major == 0 then
                        if req.version.minor != "*" then
                            req.version.minor == ver.minor
                        else true
                    else
                        req.version.major == ver.major && req.version.minor == ver.minor
                else if req.prefix == ">=" then
                    moreThan || equal
                else if req.prefix == "<=" then
                    lessThan || equal
                else if req.prefix == ">" then
                    moreThan
                else if req.prefix == "<" then
                    lessThan
                else if req.prefix == "=" then
                    equal
                else abort "Unsupported semver prefix ${req.prefix}";
        in
            if req.and != null then
                valid && (requirementMatches req.and ver)
            else if req.or != null then
                valid && (requirementMatches req.or ver)
            else
                valid
        ;

    parseSemverRequirement = input:
        let
            prefix = choose [
                (word "~")
                (word "^")
                (word ">=")
                (word "<=")
                (word ">")
                (word "<")
                (word "=")
                (word "")
            ] input;
            skipSpaces = skipEmpty prefix.input;
            version = parseSemver skipSpaces.input;

            skipEmptyComma = takeWhile (char (x: x == " " || x == "\t" || x == ","));

            orRange = andThen (andThen skipEmptyComma (word "||")) skipEmptyComma (elemAt version 1);
            andRange = skipEmptyComma (elemAt version 1);
        in {
            prefix = prefix.value;
            version = elemAt version 0;
            or = if (elemAt version 1) != null && orRange != null then
                parseSemverRequirement orRange.input
            else
                null;
            and = if (elemAt version 1) != null && orRange == null && andRange.value != "" then
                parseSemverRequirement andRange.input
            else
                null;
        };

    # "X.Y.Z-suffix other"-> [{ x = X; y = Y; z = Z; suffix = suffix;} " other"]
    parseSemver = input:
        let
            parts = match "([0-9]+|\\*)\\.?([0-9]+|\\*)?\\.?([0-9]+|\\*)?(\\-[a-zA-Z0-9\\.\\-]+)?([ ,](.*))?" input;
        in
        if parts != null then
        [
            {
                major = if (elemAt parts 0) == "*" then "*" else fromJSON (elemAt parts 0);
                minor = if (elemAt parts 1) == "*" || (elemAt parts 1) == null then "*" else fromJSON (elemAt parts 1);
                patch = if (elemAt parts 2) == "*" || (elemAt parts 2) == null then "*" else fromJSON (elemAt parts 2);
                suffix = elemAt parts 3;
            }
            (elemAt parts 5)
        ] else abort("Invalid version ${input}")
        ;
in
{
    inherit parseSemver parseSemverRequirement requirementMatches compareVersions;

    test = requirementMatches (parseSemverRequirement ">= 0.2, < 0.4")(elemAt (parseSemver "0.3.0") 0);
}