{
    pkgs ? import <nixpkgs> {},
    parse ? import ./parse.nix {lib = pkgs.lib;},
}:
with builtins;
with parse;

let
    lib = pkgs.lib;
    comment = andThen
        (char (x: x == "#"))
        (takeWhile (char (x: x != "\n")));

    skipEmptyLines = takeWhile
        (choose [comment whitespace (char (x: x == "\n"))]);

    ident = input:
        let
            nameParse = takeWhile (choose [
                (char (x: (match "[a-zA-Z0-9_\\.]" x) != null || x == "-"))
                valueString
                valueStringSingle
            ]) input;
            name = if nameParse.value != [] then
                {
                    value = concatStringsSep "" nameParse.value;
                    input = nameParse.input;
                }
            else
                null;
        in
            name
        ;

    valueNumber = input:
        let
            maybeSign = char (x: x == "+" || x == "-") input;
            parsed = takeWhile
                (char (x: (match "[0-9_]" x) != null))
                (if maybeSign == null then input else maybeSign.input)
            ;
        in
            if parsed != null && parsed.value != [] then
                {
                    value = fromJSON (concatStringsSep ""
                        (filter
                            (x: x != "_")
                            parsed.value
                        )) * (if maybeSign != null && maybeSign.value == "-" then -1 else 1);
                    input = parsed.input;
                }
            else null;

    valueString = input:
        let
            start = char (x: x == "\"") input;
            str = if start != null then escapedString "\"" start.input else null;
            end = if str != null then
                    char (x: x == "\"") str.input
                else null;
        in
            if end != null then
                {
                    value = str.value;
                    input = end.input;
                }
            else
                null;
    valueStringSingle = input:
        let
            start = char (x: x == "'") input;
            str = if start != null then escapedString "'" start.input else null;
            end = if str != null then
                    char (x: x == "'") str.input
                else null;
        in
            if end != null then
                {
                    value = str.value;
                    input = end.input;
                }
            else
                null;
    valueStringMulti = input:
        let
            start = word ''"""'' input;
            str = if start != null then escapedString "\"" start.input else null;
            end = if str != null then
                    word ''"""'' str.input
                else null;
        in
            if end != null then
                {
                    value = str.value;
                    input = end.input;
                }
            else
                null;

    valueBoolean = input:
        let
            val = choose [
                (word "true")
                (word "false")
            ] input;
        in
            if val != null then
                {
                    value = (val.value == "true");
                    input = val.input;
                }
            else
                null
        ;

    valueMap = input:
        let
            start = char (x: x == "{") input;
            vals = if start != null then
                    andThen skipEmpty valueMapValues start.input
                else null;
            end = if vals != null then
                    andThen skipEmpty (char (x: x == "}")) vals.input
                else null;
        in
            if end != null then
                {
                    value = mergeKeys vals.value;
                    input = end.input;
                }
            else
                null;
    valueMapValues = input: let
        val = keyValue "" input;
        end = if val != null then
            andThen skipEmpty (char (x: x == ",")) val.input
        else null;
    in
        if end != null then let
            next = andThen skipEmpty valueMapValues end.input;
        in
            {
                value = [val.value] ++ next.value;
                input = next.input;
            }
        else if val != null then
            {
                value = [val.value];
                input = val.input;
            }
        else
            {
                value = [];
                input = input;
            }
    ;

    valueArray = input:
        let
            start = char (x: x == "[") input;
            vals = if start != null then
                    andThen skipEmptyLines valueArrayValues start.input
                else null;
            end = if vals != null then
                    andThen skipEmptyLines (char (x: x == "]")) vals.input
                else null;
        in
            if end != null then
                {
                    value = vals.value;
                    input = end.input;
                }
            else
                null;
    valueArrayValues = input: let
        val = parseValue input;
        end = if val != null then
            andThen skipEmptyLines (char (x: x == ",")) val.input
        else null;
    in
        if end != null then let
            next = andThen skipEmptyLines valueArrayValues end.input;
        in
            {
                value = [val.value] ++ next.value;
                input = next.input;
            }
        else if val != null then
            {
                value = [val.value];
                input = val.input;
            }
        else
            {
                value = [];
                input = input;
            }
    ;

    parseValue = choose [
        valueNumber
        valueStringMulti
        valueString
        valueStringSingle
        valueBoolean
        valueArray
        valueMap
    ];

    keyValue = prefix:
        input:
        let
            key = ident input;
            parsedValue = if key != null then
                andThen
                    (andThen
                        (andThen skipEmpty (char (x: x == "=")))
                        skipEmpty)
                    parseValue
                    key.input
            else null;
        in
        if parsedValue != null then
        {
            value = {
                key = prefix + key.value;
                value = parsedValue.value;
            };
            input = parsedValue.input;
        } else null;

    section = prefix:
        input:
            let
                parsedSection = takeWhile (andThen skipEmptyLines (keyValue prefix)) input;
                empty = skipEmptyLines parsedSection.input;
                nextArraySection = andThen (word "[[") (andThen skipEmpty ident) empty.input;
                nextSection = andThen (char (x: x == "[")) (andThen skipEmpty ident) empty.input;
            in
                if nextArraySection != null then
                    let
                        endHeader = (andThen skipEmpty (word "]]")) nextArraySection.input;
                        section2 = section (nextArraySection.value + ".") endHeader.input;
                    in
                        {
                            value = parsedSection.value
                                ++ [ { key = nextArraySection.value; value = [{}];}]
                                ++ section2.value;
                            input = section2.input;
                        }

                else if nextSection != null then
                    let
                        endHeader = (andThen skipEmpty (char (x: x == "]"))) nextSection.input;
                        section2 = section (nextSection.value + ".") endHeader.input;
                    in
                        {
                            value = parsedSection.value ++ section2.value;
                            input = section2.input;
                        }
                else
                    parsedSection
            ;

    parseTOML = input: let
        root =  section "" input;
        remaining = skipEmptyLines root.input;
    in
        if remaining.input != "" then
            abort ("Failed to parse TOML file after line " +
                toString (countLines (substring 0 ((stringLength input) - (stringLength remaining.input)) input)) +
                ":\n" +
                (substring 0 30 remaining.input)
            )
        else
            mergeKeys root.value
    ;

    mergeKeys = obj:
        foldl' (a: b: nestedKeyMerge a
            (filter (x: isString x && x != "") (split "\\." b.key))
            b.value) {} obj;

    nestedKeyMerge = cur: key: val:
        if key == [] then
            if isList cur && isList val then
                cur ++ val
            else
                val
        else
            if isList cur then
                (lib.lists.sublist 0 ((length cur)-1) cur) ++ [
                    (nestedKeyMerge (lib.lists.last cur) key val)
                ]
            else
                (if cur == null then {} else cur) // {
                    ${head key} = nestedKeyMerge (
                        if cur != null && hasAttr (head key) cur then
                            cur.${head key}
                        else null
                    ) (tail key) val;
                }
        ;
in
{
    inherit debugPrint mergeKeys;
    fromTOML = if builtins ? fromTOML && false then
        input: let
            r = builtins.tryEval (builtins.fromTOML input);
        in if r.success then
            r.value
        else
            parseTOML input
    else
        parseTOML;

    test = debugPrint (parseTOML (readFile ./test.toml));
}